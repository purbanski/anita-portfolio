'use strict';

angular.module('myApp', [
  'ngRoute',
  'myApp.viewCategory',
  'myApp.viewItem',

  'myApp.version'
])

.config(['$locationProvider', '$routeProvider', function($locationProvider, $routeProvider) {
  $locationProvider.hashPrefix('!');
  $routeProvider.otherwise({redirectTo: '/cat/private_gardens'});
}]);
