'use strict';

angular.module('myApp.viewCategory', ['ngRoute'])

.config(['$routeProvider', function($routeProvider) {
  $routeProvider.when('/cat/:name', {
    templateUrl: 'view_category/category.html',
    controller: 'ViewCategory'
  });
}])

.controller('ViewCategory', function($scope, $routeParams, $http)
{
    // TODO: validate routeParams - can load any file
    var config_file = "configs/cats/" + $routeParams.name + ".json";
    $http.get(config_file)
    .then(function(res){
       var config = res.data;   
       $scope.config = config;
     });
    
    $scope.category_name = $routeParams.name;

    $scope.range = Range;
});
