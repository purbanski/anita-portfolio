var Range = {
	"step" : function(count, step) {
		var range = [];
		var step = step || 3;

		var max = Math.ceil(count / step);

		for (var i = 0; i < max; i++) {
			range.push(i * step);
		}

		return range;
	},
	
	"simple" : function(min, max, step) {
		step = step || 1;
		var input = [];
		for (var i = min; i <= max; i += step) {
			input.push(i);
		}
		return input;
	}
}
