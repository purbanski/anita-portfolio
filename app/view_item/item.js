'use strict';

angular.module('myApp.viewItem', ['ngRoute'])

.config(['$routeProvider', function($routeProvider) {
  $routeProvider.when('/item/:category_name/:item_index', {
    templateUrl: 'view_item/item.html',
    controller: 'ViewItem'
  });
}])

.controller('ViewItem', function($scope, $routeParams, $http)
{
    // TODO: validate routeParams - can load any file
    var config_file = "configs/cats/" + $routeParams.category_name + ".json";
    $http.get(config_file)
    .then(function(res){
       var config = res.data;   
       $scope.item = config.items[$routeParams.item_index];
     });
    
    $scope.range = Range;
});
